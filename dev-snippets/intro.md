# Tonow snippets

After modification to re-build the book:

```shell
jupyter-book build dev-snippets/dev-snippets
```

Check out the content pages bundled with this sample book to see more.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/Tonow%2Fdev-snippets/main)

The source repos is https://gitlab.com/Tonow/dev-snippets
