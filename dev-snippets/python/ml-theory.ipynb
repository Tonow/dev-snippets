{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ML Theory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doc links:\n",
    "\n",
    "* https://www.kaggle.com/learn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Glossaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### y\n",
    "\n",
    "prediction target"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### X\n",
    "\n",
    "Features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Underfitting\n",
    "\n",
    "When a model fails to capture important distinctions and patterns in the data, so it performs poorly even in training data, that is called underfitting\n",
    "\n",
    "> capturing spurious patterns that won't recur in the future, leading to less accurate predictions\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Overfitting\n",
    "\n",
    "where a model matches the training data almost perfectly, but does poorly in validation and other new data. On the flip side, if we make our tree very shallow, it doesn't divide up the houses into very distinct groups.\n",
    "\n",
    "> failing to capture relevant patterns, again leading to less accurate predictions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Underfitting Overfitting](https://i.imgur.com/AXSEOfI.png \"under-overfitting\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building Model\n",
    "\n",
    "The steps to building and using a model are:\n",
    "\n",
    "* **Define**: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.\n",
    "* **Fit**: Capture patterns from provided data. This is the heart of modeling.\n",
    "* **Predict**: Just what it sounds like\n",
    "* **Evaluate**: Determine how accurate the model's predictions are"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Categorical Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ordinal Encoding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Ordinal encoding** assigns each unique value to a different integer. \n",
    "*eg: This approach assumes an ordering of the categories: \"Never\" (0) < \"Rarely\" (1) < \"Most days\" (2) < \"Every day\" (3).*\n",
    "\n",
    "![Ordinal encoding](https://i.imgur.com/tEogUAr.png \"Ordinal encoding\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### One-Hot Encoding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**One-hot encoding** creates new columns indicating the presence (or absence) of each possible value in the original data. To understand this, we'll work through an example.\n",
    "\n",
    "\n",
    "![One-hot encoding](https://i.imgur.com/TW5m0aJ.png \"One-hot encoding\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "⚠️ One-hot encoding generally **does not perform well** if the categorical variable **takes on a large number of values** (*i.e., you generally won't use it for variables taking more than 15 different values*)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cross-validation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In **cross-validation**, we run our modeling process on different subsets of the data to get multiple measures of model quality.\n",
    "\n",
    "For example, we could begin by dividing the data into 5 pieces, each 20% of the full dataset. In this case, we say that we have broken the data into 5 \"**folds**\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![cross-validation](https://i.imgur.com/9k60cVA.png \"cross-validation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### When should you use cross-validation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cross-validation gives a more accurate measure of model quality, which is especially important if you are making a lot of modeling decisions. However, it can take longer to run, because it estimates multiple models (one for each fold).\n",
    "\n",
    "So, given these tradeoffs, when should you use each approach?\n",
    "\n",
    "* For small datasets, where extra computational burden isn't a big deal, you should run cross-validation.\n",
    "* For larger datasets, a single validation set is sufficient. Your code will run faster, and you may have enough data that there's little need to re-use some of it for holdout.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Leakage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Target leakage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Target leakage occurs when your predictors include data that will not be available at the time you make predictions.\n",
    "\n",
    "Eg : you want to predict who will get sick with pneumonia\n",
    "\n",
    "| got_pneumonia | age | weight | male | took_antibiotic_medicine | ... |\n",
    "|--------------|--------|----------|---------|-------------------|-----|\n",
    "| False | 65 | 100 | False | False | ... |\n",
    "| False | 72 | 130 | True | False | ... |\n",
    "| True | 58 | 100 | False | True | ... |\n",
    "\n",
    "People take antibiotic medicines after getting pneumonia in order to recover. The raw data shows a strong relationship between those columns, but `took_antibiotic_medicine` is frequently changed after the value for `got_pneumonia` is determined. This is target leakage.\n",
    "\n",
    "The model would see that anyone who has a value of False for `took_antibiotic_medicine` didn't have pneumonia.\n",
    "\n",
    "**To prevent this type of data leakage, any variable updated (or created) after the target value is realized should be excluded.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train-Test Contamination"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Wikipedia article](https://en.wikipedia.org/wiki/Leakage_(machine_learning))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Scikit-learn usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. import and save dataset 📬\n",
    "1. explore the dataset 🧐 📊\n",
    "1. clean and create a **Transformer** 🧹 https://scikit-learn.org/stable/data_transforms.html\n",
    "    * [Pre-processing](https://scikit-learn.org/stable/modules/preprocessing.html#preprocessing) \n",
    "    - [ ] [Encoder](https://scikit-learn.org/stable/modules/preprocessing.html#encoding-categorical-features) (get only numerical values) \n",
    "    - [ ] check nan value -> may be Imputation to replace missing data\n",
    "    - [ ] check outliers\n",
    "    - [ ] [Scaling](https://scikit-learn.org/stable/modules/preprocessing.html#standardization-or-mean-removal-and-variance-scaling) / [Normalize](https://scikit-learn.org/stable/modules/preprocessing.html#normalization)\n",
    "    - [ ] [Features Selection](https://scikit-learn.org/stable/modules/feature_selection.html) (ki 2 / covariance)\n",
    "    - [ ] Extract new features\n",
    "        - [ ] Clustering\n",
    "    - [ ] For continues variables : Reduce complexity (Principal Components Analysis) ⚠️ make Scaling or Normalize before\n",
    "1. choose **Estimator** 🤌 [like machine_learning_map](https://scikit-learn.org/stable/machine_learning_map.html) + hyperparameters\n",
    "    ```python\n",
    "        model = LiearRegression( ... )\n",
    "    ```\n",
    "1. Split train and test set\n",
    "    ```python\n",
    "        from sklearn.model_selection import train_test_split\n",
    "        train_X, test_X, train_y, test_y = train_test_split(X, y, random_state=1, test_size=0.2)\n",
    "    ```\n",
    "1. Train model on X, y 🏃\n",
    "    (2 numpy tables)\n",
    "    ```python\n",
    "        model.fit(train_X, train_y)\n",
    "    ```\n",
    "1. Evaluate model ✅\n",
    "    like\n",
    "    ```python\n",
    "        model.score(test_X, test_y)\n",
    "    ```\n",
    "    ⚠️ but use better **validation curve** 👇\n",
    "    1. [Cross validation](https://scikit-learn.org/stable/modules/cross_validation.html) with [validation curve](https://scikit-learn.org/stable/modules/learning_curve.html) \n",
    "        ```python\n",
    "            from sklearn.model_selection import validation_curve\n",
    "\n",
    "            train_scores, valid_scores = validation_curve(my_pipeline, train_X, train_y,\n",
    "                                        param_name=\"C\",\n",
    "                                        param_range=np.logspace(-7, 3, 3))\n",
    "        ```\n",
    "        ⚠️ but use mutch better **GridSearchCV** 👇\n",
    "        1. Search all parameter with [GridSearchCV](https://scikit-learn.org/stable/modules/grid_search.html#grid-search) \n",
    "            ```python\n",
    "                from sklearn.model_selection import GridSearchCV\n",
    "\n",
    "                parameters = {'kernel':('linear', 'rbf'), 'C':[1, 10]}\n",
    "                scoring_name = \"neg_mean_absolute_error\" # https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter\n",
    "                grid = GridSearchCV(model, parameters, cv=5, scoring=scoring_name)\n",
    "                grid.fit(train_X, train_y)\n",
    "\n",
    "                grid.best_params_\n",
    "                grid.best_score_\n",
    "                best_model = grid.best_estimator_\n",
    "\n",
    "                best_model.score(test_X, test_y)\n",
    "            ```\n",
    "1. Check / evaluate result https://scikit-learn.org/stable/modules/model_evaluation.html#the-scoring-parameter-defining-model-evaluation-rules\n",
    "    * like [confusion matrix](https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html)\n",
    "    * [Mean squared error](https://scikit-learn.org/stable/modules/model_evaluation.html#mean-squared-error)\n",
    "    * use more than only one metrics\n",
    "    * check the error distribution\n",
    "1. Check if more data will accurate the result with [learning curve](https://scikit-learn.org/stable/modules/learning_curve.html#learning-curve)\n",
    "    ```python\n",
    "        from sklearn.model_selection import learning_curve\n",
    "\n",
    "        train_sizes, train_scores, valid_scores = learning_curve(model, X_train, y_train, train_sizes=np.linspace(0.2, 1.0, 5), cv=5)\n",
    "    ```\n",
    "1. Use model 👍\n",
    "    with\n",
    "    ```python\n",
    "        model.predict(X)\n",
    "    ```\n",
    "    or\n",
    "    ```python\n",
    "        model.predict_proba(X)\n",
    "    ```\n",
    "    like\n",
    "    ```python\n",
    "        def get_predict(model, feature_0, feature_n):\n",
    "            x = np.array([feature_0, feature_n]).reshape(1, 2)\n",
    "            print(model.predict(x))\n",
    "            print(model.predict_proba(x))\n",
    "    ```\n",
    "\n",
    "👇 don't forget\n",
    "\n",
    "---\n",
    "\n",
    "🔔 🙌 use [**Piplines**](https://scikit-learn.org/stable/modules/compose.html#pipeline)\n",
    "```python\n",
    "from sklearn.model_selection import GridSearchCV\n",
    "from sklearn.pipeline import make_pipeline\n",
    "from sklearn.linear_model import SGDClassifier\n",
    "from sklearn.model_selection import train_test_split\n",
    "\n",
    "X = iris.data\n",
    "y = iris.target\n",
    "\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)\n",
    "     \n",
    "\n",
    "model = make_pipeline(PolynomialFeatures(),\n",
    "                      StandardScaler(),\n",
    "                      SGDClassifier(random_state=0))\n",
    "params = {\n",
    "    'polynomialfeatures__degree':[2, 3, 4],\n",
    "    'sgdclassifier__penalty':['l1', 'l2']\n",
    "}\n",
    "\n",
    "grid = GridSearchCV(model, param_grid=params, cv=4)\n",
    "\n",
    "grid.fit(X_train, y_train)\n",
    "     \n",
    "```"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "1dc1ed9dc5c7e06dc2e7517bb966bf5de4403786662c99840fce0ec5968743b8"
  },
  "kernelspec": {
   "display_name": "Python 3.9.7 ('dev-snippets')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
