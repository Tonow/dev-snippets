# Tonow snippets

After modification to re-build the book:

```shell
jupyter-book build dev-snippets/dev-snippets
```

Check out the content pages bundled with this sample book to see more.

The source repos is https://gitlab.com/Tonow/dev-snippets
