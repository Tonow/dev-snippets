#!/usr/bin/env python
# coding: utf-8

# # Datetime

# ## isoformat

# In[1]:


from datetime import datetime


# In[2]:


now = datetime.now()
now


# In[3]:


timespec = "hours"
now_hour = now.isoformat(timespec=timespec)
print(f"str now_hour : {now_hour}")
now_hour = datetime.fromisoformat(now_hour)
print(f"datetieme now_hour : {now_hour}")
now_hour = now.isoformat(timespec="seconds")
print(f"str now_hour seconds: {now_hour}")


# In[4]:


now2 = datetime.now()
now_hour2 = datetime.fromisoformat(now2.isoformat(timespec=timespec)).isoformat(timespec="seconds")
now_hour2


# In[5]:


from pytz import UTC


str_time = "2021-02-21T06:0sd0"
datetime.fromisoformat(str_time[:-1])

