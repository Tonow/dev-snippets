#!/usr/bin/env python
# coding: utf-8

# # GeoPandas

# In[1]:


import geopandas as gpd


# There are many, many different geospatial file formats, such as `shapefile`, `GeoJSON`, `KML`, and `GPKG`.

# ## Read file

# In[2]:


# world_loans = gpd.read_file("../../data/kiva_loans/kiva_loans.shp")


# In[3]:


# world_loans.head()

