#!/usr/bin/env python
# coding: utf-8

# # Threading vs multiprocessing

# In[1]:


import os
from multiprocessing import Pool
from functools import partial
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
from time import sleep

SEPARATOR = "-"*120


# In[2]:


def insert(value, a_list, what) :
    a_list += [value]
    print(f'{what}: {os.getpid()} : {a_list}')
    sleep(0.1)


# ## Sequential

# In[3]:



my_list = [0]
to_insert= [1,2,3,4,5,6,7,8,9]
insert_to_mylist = partial(insert, a_list = my_list, what='sequential')
list(map(insert_to_mylist, to_insert))
print(f"Sequential final = {my_list}")
print(SEPARATOR)


# ## Multiprocessing

# ### Simple pool map

# In[4]:


from multiprocessing import Pool, Process
from time import sleep
from datetime import datetime, timedelta

data_inputs = range(40)
xs = range(4)

def process_data(name):
    # print(f'name : {name}\n')
    sleep(0.1)


# In[5]:


start = datetime.now()
with Pool() as pool:                     # Create a multiprocessing Pool
    pool.map(process_data, data_inputs)  # process data_inputs iterable with pool
end = datetime.now()
print(f"time total : {end - start}")


# ### different pid and the context is different 

# In[6]:


my_list = [0]
to_insert= [1,2,3,4,5,6,7,8,9]
with ProcessPoolExecutor() as pool :
    insert_to_mylist = partial(insert, a_list = my_list, what='multiprocessing')
    pool.map(insert_to_mylist, to_insert)
print(f"Multiprocessing final = {my_list}")
print(SEPARATOR)


# ### Pool Star map with env var to debug

# In[7]:


from multiprocessing import Pool
from os import environ
from time import sleep
from datetime import datetime
from random import randint

def set_for_foos(size=10):
    for_foos = {}
    for i in range(size):
        random_number = randint(0, size)
        for_foos[str(i)] = random_number
    return for_foos

def foos():
    start = datetime.now()
    print(f'Foo start : {start}')
    for_foos = set_for_foos()
    datas = []
    args = [
        (place, number) for place, number in for_foos.items()
    ]
    if environ.get('PDB'):
        for arg in args:
            datas.append(foo(*arg))
    else:
        with Pool() as pool:
            datas = pool.starmap(foo, args)
    end = datetime.now() - start
    print(f'Time for foo --> {end}')


    return datas

def foo(place: str, number: int) -> int:
    """
    :param number: an integer
    :type number: int
    :return: factorial value of n if value, else raises an error
    :rtype: int
    """
    if number < 0:
        raise ValueError("Negative values are unsupported")

    if number < 2:
        return 1

    print(f'Place : {place}')
    sleep(0.1)

    return number * foo(place, number - 1)


# In[8]:


data = foos()
data


# ## Multithreading
# 
# can share some context 

# In[9]:



my_list = [0]
to_insert= [1,2,3,4,5,6,7,8,9]
with ThreadPoolExecutor() as pool:
    insert_to_mylist = partial(insert, a_list = my_list, what='multithreading')
    pool.map(insert_to_mylist, to_insert)
print(f"Multithreading final = {my_list}")
print(SEPARATOR)


# ## Submit

# ### Sample dl many file from a same host

# In[10]:


# Inspired from https://docs.python.org/3/library/concurrent.futures.html#threadpoolexecutor-example
# And correction with https://stackoverflow.com/questions/18466079/change-the-connection-pool-size-for-pythons-requests-module-when-in-threading/18845952#18845952

from concurrent.futures import ThreadPoolExecutor
from requests import Session, adapters
import multiprocessing
import logging
from os import environ, path, remove

def open_https_session(host: str) -> Session:
        session = Session()
        cpu_count = multiprocessing.cpu_count()
        # Change the connection pool size when in Threading
        adapter = adapters.HTTPAdapter(pool_connections=cpu_count, pool_maxsize=cpu_count, max_retries=3)
        https = 'https://'
        session.mount(https, adapter)
        session.get(f"{https}{host}")
        return session

def _file_from_https(outfile: str, url: str, session: Session) -> bool:
    with session.get(url, allow_redirects=True, stream=True) as rec:
        logging.info('request {} to {}'.format(url, outfile))
        rec.raise_for_status()
        with open(outfile, 'wb') as file:
            # Iterates over the response data
            for chunk in rec.iter_content(chunk_size=8192):
                file.write(chunk)
        logging.debug('file {} ok'.format(outfile))
    if path.getsize(outfile) == 0:
        return False
    return True

def file_from_https(outfile: str, url: str, session: Session) -> bool:
    """Retrive and copy locally file from link."""
    if not path.isfile(outfile):
        dl_completed = _file_from_https(outfile, url, session)
    else:
        logging.info('file %s already download', outfile)
        dl_completed = True
    if dl_completed is False:
        # try to dl again the file
        if path.isfile(outfile):
            remove(outfile)
        dl_completed = _file_from_https(outfile, url, session)
    if dl_completed is False:
        # In the second dl if they is alway a error raise a error
        raise FileExistsError(f"File {outfile} is empty")
    return dl_completed


URLS = [
    "https://fr.wikipedia.org/wiki/Renfrew%E2%80%94Nipissing%E2%80%94Pembroke",
    "https://fr.wikipedia.org/wiki/Croton_lachnocarpus",
    "https://fr.wikipedia.org/wiki/(21062)_Iasky",
    "https://fr.wikipedia.org/wiki/Canton_d%27Ouzom,_Gave_et_Rives_du_Neez",
]

session = open_https_session("fr.wikipedia.org")

with ThreadPoolExecutor(max_workers=None) as pool:
    for idx, file_to_dl in enumerate(URLS):
        pool.submit(file_from_https, f"/tmp/{idx}.html", file_to_dl, session)

