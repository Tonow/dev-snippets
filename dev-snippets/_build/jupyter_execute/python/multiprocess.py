#!/usr/bin/env python
# coding: utf-8

# # Multiprocessing

# ## Pool Star map with env var to debug

# In[1]:


from multiprocessing import Pool
from os import environ
from time import sleep
from datetime import datetime
from random import randint

def set_for_foos(size=10):
    for_foos = {}
    for i in range(size):
        random_number = randint(0, size)
        for_foos[str(i)] = random_number
    return for_foos

def multiprocess_foo(args):
    with Pool() as pool:
        foo_datas = pool.starmap(foo, args)
    print('Foo done')
    return foo_datas

def foos():
    start = datetime.now()
    print(f'Foo start : {start}')
    for_foos = set_for_foos()
    datas = []
    args = [
        (place, number) for place, number in for_foos.items()
    ]
    if environ.get('PDB'):
        for arg in args:
            datas.append(foo(arg[0], arg[1]))
    else:
        datas = multiprocess_foo(args)
    end = datetime.now() - start
    print(f'Time for foo --> {end}')


    return datas

def foo(place: str, number: int) -> int:
    """
    :param number: an integer
    :type number: int
    :return: factorial value of n if value, else raises an error
    :rtype: int
    """
    if number < 0:
        raise ValueError("Negative values are unsupported")

    if number < 2:
        return 1

    print(f'Place : {place}')
    sleep(0.1)

    return number * foo(place, number - 1)


# In[2]:


data = foos()
data

