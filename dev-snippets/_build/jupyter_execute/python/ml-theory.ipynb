{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ML Theory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doc links:\n",
    "\n",
    "* https://www.kaggle.com/learn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Glossaire"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### y\n",
    "\n",
    "prediction target"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### X\n",
    "\n",
    "Features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Underfitting\n",
    "\n",
    "When a model fails to capture important distinctions and patterns in the data, so it performs poorly even in training data, that is called underfitting\n",
    "\n",
    "> capturing spurious patterns that won't recur in the future, leading to less accurate predictions\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Overfitting\n",
    "\n",
    "where a model matches the training data almost perfectly, but does poorly in validation and other new data. On the flip side, if we make our tree very shallow, it doesn't divide up the houses into very distinct groups.\n",
    "\n",
    "> failing to capture relevant patterns, again leading to less accurate predictions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Underfitting Overfitting](https://i.imgur.com/AXSEOfI.png \"under-overfitting\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Building Model\n",
    "\n",
    "The steps to building and using a model are:\n",
    "\n",
    "* **Define**: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.\n",
    "* **Fit**: Capture patterns from provided data. This is the heart of modeling.\n",
    "* **Predict**: Just what it sounds like\n",
    "* **Evaluate**: Determine how accurate the model's predictions are"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Categorical Variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ordinal Encoding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Ordinal encoding** assigns each unique value to a different integer. \n",
    "*eg: This approach assumes an ordering of the categories: \"Never\" (0) < \"Rarely\" (1) < \"Most days\" (2) < \"Every day\" (3).*\n",
    "\n",
    "![Ordinal encoding](https://i.imgur.com/tEogUAr.png \"Ordinal encoding\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### One-Hot Encoding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**One-hot encoding** creates new columns indicating the presence (or absence) of each possible value in the original data. To understand this, we'll work through an example.\n",
    "\n",
    "\n",
    "![One-hot encoding](https://i.imgur.com/TW5m0aJ.png \"One-hot encoding\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "⚠️ One-hot encoding generally **does not perform well** if the categorical variable **takes on a large number of values** (*i.e., you generally won't use it for variables taking more than 15 different values*)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cross-validation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In **cross-validation**, we run our modeling process on different subsets of the data to get multiple measures of model quality.\n",
    "\n",
    "For example, we could begin by dividing the data into 5 pieces, each 20% of the full dataset. In this case, we say that we have broken the data into 5 \"**folds**\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![cross-validation](https://i.imgur.com/9k60cVA.png \"cross-validation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### When should you use cross-validation?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cross-validation gives a more accurate measure of model quality, which is especially important if you are making a lot of modeling decisions. However, it can take longer to run, because it estimates multiple models (one for each fold).\n",
    "\n",
    "So, given these tradeoffs, when should you use each approach?\n",
    "\n",
    "* For small datasets, where extra computational burden isn't a big deal, you should run cross-validation.\n",
    "* For larger datasets, a single validation set is sufficient. Your code will run faster, and you may have enough data that there's little need to re-use some of it for holdout.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Leakage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Target leakage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Target leakage occurs when your predictors include data that will not be available at the time you make predictions.\n",
    "\n",
    "Eg : you want to predict who will get sick with pneumonia\n",
    "\n",
    "| got_pneumonia | age | weight | male | took_antibiotic_medicine | ... |\n",
    "|--------------|--------|----------|---------|-------------------|-----|\n",
    "| False | 65 | 100 | False | False | ... |\n",
    "| False | 72 | 130 | True | False | ... |\n",
    "| True | 58 | 100 | False | True | ... |\n",
    "\n",
    "People take antibiotic medicines after getting pneumonia in order to recover. The raw data shows a strong relationship between those columns, but `took_antibiotic_medicine` is frequently changed after the value for `got_pneumonia` is determined. This is target leakage.\n",
    "\n",
    "The model would see that anyone who has a value of False for `took_antibiotic_medicine` didn't have pneumonia.\n",
    "\n",
    "**To prevent this type of data leakage, any variable updated (or created) after the target value is realized should be excluded.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train-Test Contamination"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Wikipedia article](https://en.wikipedia.org/wiki/Leakage_(machine_learning))"
   ]
  }
 ],
 "metadata": {
  "interpreter": {
   "hash": "fd63fdab7f40f8b93de9751aa3bba11571f1fdaf840bfa52395ecb03216021ca"
  },
  "kernelspec": {
   "display_name": "dev-snippets",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}