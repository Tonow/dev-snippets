#!/usr/bin/env python
# coding: utf-8

# # Data exploration

# 
# 1) Understand the problem. We'll look at each variable and do a philosophical analysis about their meaning and importance for this problem.
# 1) Univariable study. We'll just focus on the dependent variable ('SalePrice') and try to know a little bit more about it.
# 1) Multivariate study. We'll try to understand how the dependent variable and independent variables relate.
# 1) Basic cleaning. We'll clean the dataset and handle the missing data, outliers and categorical variables.
# 1) Test assumptions. We'll check if our data meets the assumptions required by most multivariate techniques.
# 
# 
# other notes are on https://www.kaggle.com/pmarcelino/comprehensive-data-exploration-with-python
# 

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from scipy import stats
import warnings
warnings.filterwarnings('ignore')
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


df_train = pd.read_csv('../../data/melb_data.csv')


# In[3]:


df_train.dtypes

import csv

data_info_csv_path = '../../data/data-info.csv'
with open(data_info_csv_path, 'w') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    filewriter.writerow(['Name', 'Types', 'Segment', 'Expectation', 'Conclusion', 'Comments'])
    for column in df_train.columns:
        filewriter.writerow([column, df_train[column].dtypes])

pd.read_csv(data_info_csv_path)


# In[4]:


df_train.describe()


# In[5]:


df_train.head()


# # Visualisation

# ## Seaborn

# In[6]:




# Set the width and height of the figure
plt.figure(figsize=(16,6))
plt.title("Super title")

loc_max = 200
sns.lineplot(data=df_train.iloc[0:loc_max].Price, label="Price")
sns.lineplot(data=df_train.iloc[0:loc_max].Landsize, label="Landsize")


# In[7]:


# Set the width and height of the figure
plt.figure(figsize=(16,6))
plt.title("Super title")

loc_max = 200

df_price = df_train.iloc[0:loc_max].Price
df_landsize = df_train.iloc[0:loc_max].Landsize

ax = sns.lineplot(data=df_price, label="Price in $", legend=False)
# Create a second y-axis with the scaled ticks
ax2 = ax.twinx()

sns.lineplot(data=df_landsize, label="Landsize", legend=False, color="r")
ax.figure.legend()


# In[8]:


plt.figure(figsize=(10,6))


# Bar chart relation landsize and price
sns.barplot(x=df_price, y=df_landsize)


# In[9]:




plt.figure(figsize=(14,7))


heatmap_df = df_train[["Price", "Lattitude", "Longtitude"]][0:20]
# heatmap_df
heatmap_df_piv = heatmap_df.pivot(index="Lattitude", columns='Longtitude', values='Price')

# Heatmap showing average arrival delay for each airline by month
sns.heatmap(data=heatmap_df_piv, annot=True)


# In[10]:


sns.scatterplot(y=df_train.Price, x=df_train.BuildingArea)


# In[11]:


# whitout outliers
q_low = df_train["Price"].quantile(0.01)
q_hi  = df_train["Price"].quantile(0.99)
df_filtered = df_train[(df_train["Price"] < q_hi) & (df_train["Price"] > q_low)]

q_low = df_filtered["BuildingArea"].quantile(0.01)
q_hi  = df_filtered["BuildingArea"].quantile(0.99)
df_filtered_2 = df_filtered[(df_filtered["BuildingArea"] < q_hi) & (df_filtered["BuildingArea"] > q_low)]


# In[12]:


sns.scatterplot(y=df_filtered_2.Price, x=df_filtered_2.BuildingArea)


# In[13]:


sns.regplot(x=df_filtered_2.BuildingArea, y=df_filtered_2.Price, marker="+")


# In[14]:


sns.scatterplot(x=df_filtered_2.BuildingArea, y=df_filtered_2.Price, hue=df_filtered_2.Rooms)


# In[15]:


sns.lmplot(x="BuildingArea", y="Price", hue="Rooms", data=df_filtered_2)


# In[16]:


nb_rows = 2000
df_filtered_2_cut = df_filtered_2.iloc[:nb_rows]

sns.swarmplot(x=df_filtered_2_cut.Rooms, y=df_filtered_2_cut.Price)


# ## Distribution

# In[17]:


sns.distplot(a=df_filtered_2_cut.Rooms, kde=False)


# ### Density

# In[18]:


sns.kdeplot(data=df_filtered_2_cut.Price, shade=True)


# In[19]:


sns.jointplot(x=df_filtered_2_cut.Rooms, y=df_filtered_2_cut.Price, kind="kde")


# ### Color-coded plots

# In[20]:


bathroom_1 = df_filtered_2_cut.where(df_filtered_2_cut["Bathroom"] == 1)
bathroom_2 = df_filtered_2_cut.where(df_filtered_2_cut["Bathroom"] == 2)
bathroom_3 = df_filtered_2_cut.where(df_filtered_2_cut["Bathroom"] == 3)
bathroom_4 = df_filtered_2_cut.where(df_filtered_2_cut["Bathroom"] == 4)

sns.distplot(a=bathroom_1.Price, label="bathroom_1")
sns.distplot(a=bathroom_2.Price, label="bathroom_2")
sns.distplot(a=bathroom_3.Price, label="bathroom_3")
sns.distplot(a=bathroom_4.Price, label="bathroom_4")

plt.title("Price by bathroom nb")

plt.legend()

