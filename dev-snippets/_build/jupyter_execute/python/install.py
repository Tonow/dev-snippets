#!/usr/bin/env python
# coding: utf-8

# # Install

# ## conda
# 
# * `conda create --name dev-snippets`
# * `conda install --yes --file  dev-snippets/python/requirements.txt`
# * `conda activate dev-snippets`

# ## virtualenv
# 
# * `virtualenv env`
# * `source env/bin/activate`
# * `pip install -r requirements-jb.txt`
# * `pip install -r  dev-snippets/python/requirements.txt`

# In[1]:


import pandas as pd

requirements = pd.read_csv('../../requirements.txt', sep=" ", header=None)
for requirement in requirements[0].values:
    print(requirement)

