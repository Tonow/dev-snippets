#!/usr/bin/env python
# coding: utf-8

# # Xarray

# In[1]:


import xarray as xr
# import cfgrib
print(f"xr version : {xr.__version__}")
# print(f"cfgrib version : {cfgrib.__version__}")


# In[2]:


ds_path = "../../data/2021090200.grib"


# ## Open grib file

# ### Global approche to open grib

# In[3]:


# dataset without u10 cause : 
#     skipping variable: paramId==165 shortName='u10'
#     cfgrib.dataset.DatasetBuildError: key present and new value is different: key='step' value=Variable(dimensions=('step',)
ds_without_u10 = xr.open_dataset(ds_path, engine="cfgrib")
ds_without_u10


# ### Open grib file with filter_by_keys

# In[4]:


# dataset with u10
ds_u10 = xr.open_dataset(ds_path, engine="cfgrib", backend_kwargs={'filter_by_keys': {'paramId': 165}})
ds_u10


# ## Merge dataset

# In[5]:


ds_global = xr.merge([ds_without_u10, ds_u10])
ds_global


# In[6]:


ds_global.u10[-2]

