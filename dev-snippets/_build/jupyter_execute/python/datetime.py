#!/usr/bin/env python
# coding: utf-8

# # [Datetime](https://docs.python.org/3/library/datetime.html#)

# ## isoformat

# In[1]:


from datetime import datetime


# In[2]:


now = datetime.now()
now


# In[3]:


timespec = "hours"
now_hour = now.isoformat(timespec=timespec)
print(f"str now_hour : {now_hour}")
now_hour = datetime.fromisoformat(now_hour)
print(f"datetieme now_hour : {now_hour}")
now_hour = now.isoformat(timespec="seconds")
print(f"str now_hour seconds: {now_hour}")


# In[4]:


now2 = datetime.now()
now_hour2 = datetime.fromisoformat(now2.isoformat(timespec=timespec)).isoformat(timespec="seconds")
now_hour2


# ### [fromisoformat](https://docs.python.org/3/library/datetime.html#datetime.datetime.fromisoformat)

# In[5]:


str_time = "2021-02-21T06:00z"
datetime.fromisoformat(str_time[:-1])


# ## [TimeDelta](https://docs.python.org/3/library/datetime.html#timedelta-objects)

# In[6]:


from datetime import timedelta


# In[7]:


now - timedelta(hours=10, minutes=50)


# ## [strftime() and strptime()](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior)

# ### [Format code](https://docs.python.org/3/library/datetime.html#strftime-and-strptime-format-codes)

# In[8]:


str_now = now.strftime("%Y-%m")
str_now


# In[9]:


datetime.strptime(str_now, "%Y-%M")


# # [Pytz](http://pytz.sourceforge.net/)
# 

# ## Set time zone with astimezone and pytz

# In[10]:


from pytz import UTC

now.astimezone(UTC)


# # [Dateutil](https://dateutil.readthedocs.io/en/stable/) 

# ## [Relativedelta](https://dateutil.readthedocs.io/en/stable/relativedelta.html)

# In[11]:


from dateutil.relativedelta import relativedelta

now - relativedelta(months=3)

