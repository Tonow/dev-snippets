#!/usr/bin/env python
# coding: utf-8

# # ML Theory

# Doc links:
# 
# * https://www.kaggle.com/learn

# ## Glossaire

# ### y
# 
# prediction target

# ### X
# 
# Features

# ### Underfitting
# 
# When a model fails to capture important distinctions and patterns in the data, so it performs poorly even in training data, that is called underfitting
# 
# > capturing spurious patterns that won't recur in the future, leading to less accurate predictions
# 

# ### Overfitting
# 
# where a model matches the training data almost perfectly, but does poorly in validation and other new data. On the flip side, if we make our tree very shallow, it doesn't divide up the houses into very distinct groups.
# 
# > failing to capture relevant patterns, again leading to less accurate predictions.

# ![Underfitting Overfitting](https://i.imgur.com/AXSEOfI.png "under-overfitting")

# ## Method

# ### Building Model
# 
# The steps to building and using a model are:
# 
# * **Define**: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.
# * **Fit**: Capture patterns from provided data. This is the heart of modeling.
# * **Predict**: Just what it sounds like
# * **Evaluate**: Determine how accurate the model's predictions are

# ## Categorical Variables

# ### Ordinal Encoding

# **Ordinal encoding** assigns each unique value to a different integer. 
# *eg: This approach assumes an ordering of the categories: "Never" (0) < "Rarely" (1) < "Most days" (2) < "Every day" (3).*
# 
# ![Ordinal encoding](https://i.imgur.com/tEogUAr.png "Ordinal encoding")

# ### One-Hot Encoding

# **One-hot encoding** creates new columns indicating the presence (or absence) of each possible value in the original data. To understand this, we'll work through an example.
# 
# 
# ![One-hot encoding](https://i.imgur.com/TW5m0aJ.png "One-hot encoding")

# ⚠️ One-hot encoding generally **does not perform well** if the categorical variable **takes on a large number of values** (*i.e., you generally won't use it for variables taking more than 15 different values*).

# ## Cross-validation

# In **cross-validation**, we run our modeling process on different subsets of the data to get multiple measures of model quality.
# 
# For example, we could begin by dividing the data into 5 pieces, each 20% of the full dataset. In this case, we say that we have broken the data into 5 "**folds**".

# ![cross-validation](https://i.imgur.com/9k60cVA.png "cross-validation")

# ### When should you use cross-validation?

# Cross-validation gives a more accurate measure of model quality, which is especially important if you are making a lot of modeling decisions. However, it can take longer to run, because it estimates multiple models (one for each fold).
# 
# So, given these tradeoffs, when should you use each approach?
# 
# * For small datasets, where extra computational burden isn't a big deal, you should run cross-validation.
# * For larger datasets, a single validation set is sufficient. Your code will run faster, and you may have enough data that there's little need to re-use some of it for holdout.
# 

# ## Data Leakage

# ### Target leakage

# Target leakage occurs when your predictors include data that will not be available at the time you make predictions.
# 
# Eg : you want to predict who will get sick with pneumonia
# 
# | got_pneumonia | age | weight | male | took_antibiotic_medicine | ... |
# |--------------|--------|----------|---------|-------------------|-----|
# | False | 65 | 100 | False | False | ... |
# | False | 72 | 130 | True | False | ... |
# | True | 58 | 100 | False | True | ... |
# 
# People take antibiotic medicines after getting pneumonia in order to recover. The raw data shows a strong relationship between those columns, but `took_antibiotic_medicine` is frequently changed after the value for `got_pneumonia` is determined. This is target leakage.
# 
# The model would see that anyone who has a value of False for `took_antibiotic_medicine` didn't have pneumonia.
# 
# **To prevent this type of data leakage, any variable updated (or created) after the target value is realized should be excluded.**

# ### Train-Test Contamination

# [Wikipedia article](https://en.wikipedia.org/wiki/Leakage_(machine_learning))
