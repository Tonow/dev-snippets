#!/usr/bin/env python
# coding: utf-8

# # Numpy

# In[1]:


import numpy as np


# ## Tips

# ### Ellipsis

# In[2]:


x = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
x[...,0]


# In[3]:


x = np.array([[[1],[2],[3]], [[4],[5],[6]]])
x[...,0]

