#!/usr/bin/env python
# coding: utf-8

# # Test

# ## Exception

# In[1]:


import unittest

class classname(unittest.TestCase):
    """
    classname tests
    """

    def setUp(self):
        pass


    def tearDown(self):
        pass

    def test_raise(self):
        """
        method test
        """
        with self.assertRaises(ZeroDivisionError):
            3 / 0


# ## Pytest

# ### Show logging

# To show the logging to INFO and upper.
# 
# ```shell
# pytest tests/test_super_cools.py --log-cli-level=INFO
# ```

# ### Add PDB in error

# ```shell
# pytest --pdb tests/test_super_cools.py
# ```

# ### Run a particular method

# ```shell
# pytest tests/test_super_cools.py::CoolClassTests::test_cool
# ```
