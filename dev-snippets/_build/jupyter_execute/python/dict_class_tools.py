#!/usr/bin/env python
# coding: utf-8

# # Dict

# ## Nested dict

# In[1]:


data_dict = {
    "a": {
        "d": "d",
        "i": "i",
        "e": {
            "e": {
                "g": "g",
                "i": "i",
                "h": {
                    "i": "i"
                }
            }
        }
    }, 
    "b": "b",
    "i": "i",
}

data_dict


# In[2]:


def print_nested_dict(dict_obj, indent = 1):
    ''' Pretty Print nested dictionary with given indent level  
    '''
    # Iterate over all key-value pairs of dictionary
    for key, value in dict_obj.items():
        # If value is dict type, then print nested dict 
        if isinstance(value, dict):
            print(' ' * indent, key, ':', '{')
            print_nested_dict(value, indent + 4)
            print(' ' * indent, '}')
        else:
            print(' ' * indent, key, ':', value)


# In[3]:


def display_dict(dict_obj):
    ''' Pretty print nested dictionary
    '''
    print('{')
    print_nested_dict(dict_obj)
    print('}')


# In[4]:


display_dict(data_dict)


# In[5]:


from functools import reduce
import operator

def nested_dict_pairs_iterator(dict_obj):
    ''' This function accepts a nested dictionary as argument
        and iterate over all values of nested dictionaries
    '''
    # Iterate over all key-value pairs of dict argument
    for key, value in dict_obj.items():
        # Check if value is of dict type
        if isinstance(value, dict):
            # If value is dict then iterate over all its values
            for pair in  nested_dict_pairs_iterator(value):
                yield (key, *pair)
        else:
            # If value is not dict type then yield the value
            yield (key, value)


# In[6]:


def get_by_path(root, items):
    """Access a nested object in root by item sequence."""
    return reduce(operator.getitem, items, root)

def set_by_path(root, items, value):
    """Set a value in a nested object in root by item sequence."""
    get_by_path(root, items[:-1])[items[-1]] = value


# In[7]:


for pair in nested_dict_pairs_iterator(data_dict):
    pairs = list(pair[:-1])
    to_test = get_by_path(data_dict, pairs)
    print(pair)
    
    print("_".join(pair[:-1]))
        
    if to_test == "i":
        set_by_path(data_dict, pairs, "not a I")


# In[8]:


display_dict(data_dict)


# ## Call static method with dict

# In[9]:


from pathlib import Path
import json
# import yaml

class MyClass(dict):
    """Class that reads the contents of the config file and
    returns the loaded dictionnary as a result with some customisation
    """

    @classmethod
    def load(cls, config_file: Path):

        extentions_readers = {
            '.json': MyClass._load_json_file,
            '.yaml': MyClass._load_yaml_file,
            '.yml': MyClass._load_yaml_file,
        }
        read_function = extentions_readers[config_file.suffix]
        config = read_function(config_file)

        return cls(config)

    @staticmethod
    def _load_json_file(config_file: Path) -> dict:
        with open(config_file, 'r') as json_file:
            config = json.load(json_file)
        return config

    
    @staticmethod
    def _load_yaml_file(config_file: Path) -> dict:
        print('you should add pyyaml package')
        return False
#         with open(config_file, 'r') as stream:
#             yaml_file = stream.read()


# In[10]:


json_file_path =  Path('result.json')

from os import remove

with open(json_file_path, 'w') as json_file:
    json.dump(data_dict, json_file)
    
json_file = MyClass.load(json_file_path)
json_file

remove(json_file_path)

