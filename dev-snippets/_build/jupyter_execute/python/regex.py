#!/usr/bin/env python
# coding: utf-8

# # [Regex](https://docs.python.org/3/howto/regex.html)

# In[1]:


import re


# ## Match

# In[2]:


dims = ["a", "b", "baba", "bar", "foo"]

regexs = ["ba.*", "ba*"]
for regex in regexs:
    print(f"Match to {regex}:")
    for dim in dims:
        if re.match(rf'{regex}', dim):
            print(f"\t{dim}")

