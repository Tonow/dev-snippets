#!/usr/bin/env python
# coding: utf-8

# # Path

# ## path or pathlib

# Correspondence table https://docs.python.org/3/library/pathlib.html#correspondence-to-tools-in-the-os-module

# ## urlpars

# In[1]:


# url to path

from urllib.parse import urlparse

url = "http://localhost:8888/lab"
url_parse = urlparse(url)
url_parse


# In[2]:


url_parse.path


# In[3]:


url = "gs://sungeodata/test/lab"
url_parse = urlparse(url)
url_parse


# In[4]:


url = "gs://sungeodata/test/lab"
path = urlparse(url).path
path


# In[5]:


url = "coucou je ne suis pas une url"
url_parse = urlparse(url)
url_parse


# ## Path exists islink symlink

# In[6]:


from os import path, symlink, listdir, readlink
import os
import pandas as pd

train_csv_path = "../../data/melb_data.csv"
iowa_file_path = '../../data/train_2.csv'

print(listdir('../../data'))
print(path.exists(iowa_file_path))
print(path.islink(iowa_file_path))
if not path.exists(iowa_file_path) and not path.islink(iowa_file_path):
    os.symlink(train_csv_path, iowa_file_path)


# ## Path readlink

# In[7]:



print(readlink(iowa_file_path))
print(path.exists(readlink(iowa_file_path)))
data_csv = pd.read_csv(readlink(iowa_file_path))

