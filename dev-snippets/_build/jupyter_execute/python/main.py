#!/usr/bin/env python
# coding: utf-8

# # Main

# ## Logging

# ### Format

# In[1]:


import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y/%m/%d %I:%M:%S %p')


# ### Set level for a lib

# In[2]:


import numpy

logging.getLogger('numpy').setLevel(logging.WARNING)


# ## With arguments

# To use with 
# 
# ```shell
# python main.py -lat 3.2 -config appsunclim/data/static/config.json --months 2 -p param1 param2
# ```

# In[3]:


import argparse
import sys
import datetime


def valid_date(date_to_check):
    """Function to transform to datetime valid the input string
    """
    try:
        return datetime.strptime(date_to_check, "%Y-%m-%d")
    except ValueError:
        msg = "not a valid date: {!r}".format(date_to_check)
        raise argparse.ArgumentTypeError(msg)

def parse_args(args: list) -> argparse.Namespace:

    parser = argparse.ArgumentParser(
        description='Launches a Sungeodata run', formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )


    parser.add_argument(
        '--string_date',
        '-s_date',
        type=valid_date,
        default=datetime.now().strftime("%Y-%m-%d"),
        required=False,
        help='The date with format %Y-%m-%d',
    )

    parser.add_argument(
        '--latitude',
        '-lat',
        type=float,
        required=True,
        help='this is help text',
    )

    parser.add_argument(
        '--local_config_path',
        '-config',
        type=str,
        required=False,
        help='this is help text',
    )

    parser.add_argument(
        '--months',
        '-m',
        type=int,
        default=360,
        required=False,
        help='this is help text',
    )

    parser.add_argument(
        '--last_month',
        '-last_m',
        type=str,
        default="1994-12",
        required=False,
        help='this is help text',
    )

    parser.add_argument(
        '--params',
        '-p',
        nargs='+',
        required=True,
        help='this is help text',
    )

    return parser.parse_args(args)

def main(latitude, local_config_path, dates_params, params):
    print(latitude)
    print(local_config_path)
    print(dates_params)
    print(params)


if __name__ == "__main__":
    arguments = parse_args(sys.argv[1:])
    local_config_path = arguments.local_config_path
    params = arguments.params
    dates_params = {
        "last_month": arguments.last_month,
        "months": arguments.months,
    }
    main(arguments.latitude, local_config_path, dates_params, params)

