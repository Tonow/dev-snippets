#!/usr/bin/env python
# coding: utf-8

# # [Pandas](https://pandas.pydata.org/pandas-docs/stable/user_guide/index.html)

# In[1]:


import pandas as pd


# ## DataFrame

# In[2]:


# A dataframe is a dict of list 

pd.DataFrame({'Yes': [50, 21], 'No': [131, 2]})


# In[3]:


# the index can be change

pd.DataFrame({'Bob': ['I liked it.', 'It was awful.'], 
              'Sue': ['Pretty good.', 'Bland.']},
             index=['Product A', 'Product B'])


# ## Series

# In[4]:


# Like a list or a numpy arrais but we can add a index 

pd.Series([30, 35, 40], index=['2015 Sales', '2016 Sales', '2017 Sales'], name='Product A')


# ## Read csv

# In[5]:


melbourne_file_path = '../../data/melb_data.csv'
melbourne_data = pd.read_csv(melbourne_file_path) 


# In[6]:


pd.read_csv(melbourne_file_path, index_col="Address") 


# ## Dtype

# In[7]:


melbourne_data.dtypes


# In[8]:


melbourne_data.Landsize.astype('int16')


# ## Missing data

# In[9]:


melbourne_data[pd.isnull(melbourne_data.YearBuilt)]


# ### Replace missing values

# In[10]:


melbourne_data.YearBuilt.fillna("Unknown")


# ## Drops missing values

# In[11]:


# dropna drops missing values (think of na as "not available")
melbourne_data = melbourne_data.dropna(axis=0)


# ## Replace some values

# In[12]:


melbourne_data.CouncilArea.replace("Yarra", "Yarra_council")


# ## Get columns list

# In[13]:


melbourne_data.columns


# ## Select column

# In[14]:


# One column
y = melbourne_data.Price

# List of columns

melbourne_features = ['Rooms', 'Bathroom', 'Landsize', 'Lattitude', 'Longtitude']
X = melbourne_data[melbourne_features]


# ## Show info on data

# In[15]:


X.describe()


# In[16]:


X.head()


# In[17]:


X.Rooms.unique()


# In[18]:


X.Rooms.value_counts()


# ## Indexing

# In[19]:


X


# In[20]:


X.reset_index()


# ### Get one line by index iloc

# In[21]:


X.iloc[0]


# ### Get one column or part of by index iloc

# In[22]:


X.iloc[:, 0]


# In[23]:


X.iloc[2:5, 0]


# In[24]:


X.iloc[[2, 3, 5]]


# ## loc Vs iloc

# When choosing or transitioning between loc and iloc, there is one "gotcha" worth keeping in mind, which is that the two methods use slightly different indexing schemes.
# 
# iloc uses the Python stdlib indexing scheme, where the first element of the range is included and the last one excluded. So 0:10 will select entries 0,...,9. loc, meanwhile, indexes inclusively. So 0:10 will select entries 0,...,10.
# 
# Why the change? Remember that loc can index any stdlib type: strings, for example. If we have a DataFrame with index values Apples, ..., Potatoes, ..., and we want to select "all the alphabetical fruit choices between Apples and Potatoes", then it's a lot more convenient to index df.loc['Apples':'Potatoes'] than it is to index something like df.loc['Apples', 'Potatoet'] (t coming after s in the alphabet).
# 
# This is particularly confusing when the DataFrame index is a simple numerical list, e.g. 0,...,1000. In this case df.iloc[0:1000] will return 1000 entries, while df.loc[0:1000] return 1001 of them! To get 1000 elements using loc, you will need to go one lower and ask for df.loc[0:999].
# 
# Otherwise, the semantics of using loc are the same as those for iloc.

# In[25]:


X.loc[0:5, ["Rooms", "Lattitude", "Longtitude"]]


# In[26]:


X.iloc[0:3][["Rooms", "Lattitude", "Longtitude"]]


# ## Create smaller dataframe with only usefull columns

# In[27]:


X[["Rooms", "Lattitude", "Longtitude"]]


# ## Conditional selection

# In[28]:


X.loc[(X.Lattitude < -38.15) & (X.Longtitude > 144.99)]


# In[29]:


X.loc[X.Rooms.isin([7,8,9,10])]


# In[30]:


X.loc[X.Rooms.isin([7,8,9,10]) & (X.Bathroom >= 4)]


# ## Function and maps

# `map()` and `apply()` return new, transformed Series and DataFrames, respectively. They don't modify the original data they're called on. If we look at the first row of reviews, we can see that it still has its original points value.

# In[31]:


landsize_max = X.Landsize.max()
landsize_max


# ### Map

# In[32]:


X.Landsize.map(lambda p: p - landsize_max)


# ### Apply

# In[33]:


def remax_landsize(row):
    row.Landsize = row.Landsize - landsize_max
    return row

X.apply(func=remax_landsize, axis="columns")


# In[34]:


def remax_landsize(row):
    if row.Rooms > 6:
        return "aa"
    elif row.Landsize >= 10000:
        return "a"
    elif row.Landsize >= 1000:
        return "b"
    else:
        return "c"
X.apply(func=remax_landsize, axis="columns")


# In[35]:


def remax_landsize(col):
    if col.name == "Landsize":
        col = col - landsize_max
    return col

X.apply(func=remax_landsize, axis="index")


# In[36]:


def remax_landsize_with_args(col, landsize_max):
    if col.name == "Landsize":
        col = col - landsize_max
    return col

X.apply(func=remax_landsize_with_args, axis="index", args=[landsize_max])


# ### Build in operator

# These operators should be faster than `map()` or `apply()` because they use speed ups built into pandas. All of the standard Python operators (>, <, ==, and so on) work in this manner.
# 
# However, they are not as flexible as `map()` or `apply()`, which can do more advanced things, like applying conditional logic, which cannot be done with addition and subtraction alone.

# In[37]:


X.Landsize - landsize_max


# In[38]:


bargain_idx = (X.Landsize / X.Rooms).idxmax()
bargain_idx


# In[39]:


X.loc[bargain_idx]


# ### count values type

# In[40]:


X.Longtitude.map(lambda longi: longi > 144.999 ).sum()


# In[41]:


X.Longtitude.map(lambda longi: "999" in str(longi) ).sum()


# ## Grouping

# In[42]:


X.groupby("Rooms").Rooms.count()


# In[43]:


# same as
X.groupby("Rooms").size()


# In[44]:


# little difference with value_counts
X.Rooms.value_counts()


# In[45]:


X.groupby("Rooms").Landsize.min()


# ### Apply on groupby

# In[46]:


X.groupby(["Rooms", "Bathroom"]).apply(lambda df: df.loc[df.Landsize.idxmax()])


# In[47]:


X.groupby(["Rooms"]).apply(lambda df: df.loc[(df.Landsize >= 4.0) & (df.Landsize <= 3300.0)])


# In[48]:


X.groupby(['Rooms']).Landsize.agg([len, min, max])


# #### [MultiIndex](https://pandas.pydata.org/pandas-docs/stable/user_guide/advanced.html)

# In[49]:


X.groupby(["Rooms", "Bathroom"]).Landsize.agg([len, min, max])


# ## Sorting

# In[50]:


X.sort_values(by=["Landsize", "Rooms"], ascending=False)


# In[51]:


X.groupby("Rooms")['Bathroom'].max().sort_index()


# In[52]:


X.groupby(["Rooms", "Bathroom"]).size().sort_values( ascending=False)


# ## Renaming

# In[53]:


X.rename(columns={'Lattitude': 'lat', 'Longtitude': 'lon'})


# In[54]:


X.rename(index={0: 'first', 1: "sec"})


# In[55]:


X.rename_axis("house_nb", axis='rows').rename_axis("fields", axis='columns')


# ## [Combining](https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html#)

# ### concat

# In[56]:


X1 = X.iloc[0:10]
X1


# In[57]:


X2 = X.iloc[10:12]
X2


# In[58]:


pd.concat([X1, X2])


# In[59]:


X3 = X.iloc[8:12]
X3


# In[60]:


## with this concate they is duplicate values

pd.concat([X2, X3])


# ### Join

# In[61]:


X2 = X2.set_index(X2.index.set_names('house_nb'))
X3 = X3.set_index(X3.index.set_names('house_nb'))
X3


# In[62]:


X2.join(X3, lsuffix='_X2', rsuffix='_X3')


# ### Merge

# In[63]:


pd.merge(X2, X3, how="outer")


# In[64]:


pd.merge(X2, X3, how="inner")


# In[65]:


pd.merge(X2, X3, how="left", on=["Bathroom"])


# In[66]:


pd.merge(X2, X3, how="right", on=["Bathroom"])


# In[67]:


pd.merge(X2, X3, how="right", validate="one_to_many")

