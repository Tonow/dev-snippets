#!/usr/bin/env python
# coding: utf-8

# # Terraform

# ## Gitlab CI

# ### In Google cloud

# ``` yaml
# include:
#   - template: Terraform.gitlab-ci.yml
# 
# variables:
#   # If not using GitLab's HTTP backend, remove this line and specify TF_HTTP_* variables
#   TF_STATE_NAME: default
#   TF_CACHE_KEY: default
#   # If your terraform files are in a subdirectory, set TF_ROOT accordingly
#   # TF_ROOT: terraform/production
# 
# before_script:
#   - export GOOGLE_CREDENTIALS=$(echo $BASE64_GOOGLE_CREDENTIALS | base64 -d)
# 
# format:
#   stage: validate
#   script: 
#     - terraform fmt -check 
# ```

# ## Loop on values

# Eg for gcs buckets

# `main.tf`
# 
# ```json
# // Configure the Google Cloud provider
# provider "google" {
#   project = var.project
# }
# 
# # This node pool "architecture" is freely inspired from :
# # https://medium.com/google-cloud/scale-your-kubernetes-cluster-to-almost-zero-with-gke-autoscaler-9c78051cbf40
# # The existence of the default node is needed because of this limitation/known issue :
# # https://cloud.google.com/kubernetes-engine/docs/concepts/cluster-autoscaler?hl=fr#known_issues
# 
# resource "google_storage_bucket" "my-buckets" {
#   for_each                    = var.buckets
#   name                        = each.key
#   location                    = each.value.location
#   force_destroy               = each.value.force_destroy
#   storage_class               = each.value.storage_class
#   uniform_bucket_level_access = each.value.uniform_bucket_level_access
#   labels = {
#     bucket-name = each.key
#   }
# 
#   retention_policy {
#     retention_period = each.value.retention_policy.retention_period
#   }
# 
#   dynamic "lifecycle_rule" {
#     for_each = each.value.lifecycle_rules
#     content {
#       condition {
#         age = lifecycle_rule.value.condition.age
#       }
#       action {
#         type          = lifecycle_rule.value.action.type
#         storage_class = lifecycle_rule.value.action.storage_class
#       }
#     }
#   }
# }
# ```

# `variables.tf`
# 
# ```json
# variable "project" {
#   default = "mycloud"
# }
# 
# variable "description" {
#   default = "GKE mycloud gcs-buckets in europe-west1-b"
# }
# 
# 
# // Buckets list
# 
# ## type can be:
# # STANDARD, MULTI_REGIONAL, REGIONAL, NEARLINE, COLDLINE, ARCHIVE
# # https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket#storage_class
# 
# variable "buckets" {
#   description = "Map of buckets names to with the storage class."
#   type        = map(any)
#   default = {
#     bucket-1 = {
#       force_destroy               = false
#       storage_class               = "STANDARD",
#       uniform_bucket_level_access = true,
#       location                    = "EUROPE-WEST1",
#       lifecycle_rules = [
#         {
#           condition = {
#             age = 15
#           },
#           action = {
#             storage_class = "COLDLINE"
#             type          = "SetStorageClass"
#           }
#         },
#         {
#           condition = {
#             age = 1200
#           },
#           action = {
#             type          = "Delete"
#             storage_class = null
#           }
#         },
#       ],
#       retention_policy = {
#         retention_period = 1
#       }
#     },
#     bucket-2 = {
#       force_destroy               = false
#       storage_class               = "STANDARD",
#       uniform_bucket_level_access = true,
#       location                    = "EUROPE-WEST1",
#       lifecycle_rules             = [],
#       retention_policy = {
#         retention_period = 1
#       }
#     }
#   }
# }
# ```
# 
