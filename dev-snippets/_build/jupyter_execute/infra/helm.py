#!/usr/bin/env python
# coding: utf-8

# # Helm

# ## Template link

# * [Offical functions list](https://helm.sh/docs/chart_template_guide/function_list/)
# * [Offical variables doc](https://helm.sh/docs/chart_template_guide/variables/)
# * [Go template functions 1](https://docs.gomplate.ca/functions/strings/)
# * [Go template functions 2](https://masterminds.github.io/sprig/)

# ## Test template yaml

# ```shell
# helm upgrade my-deployment path/to-my/helm-charts --values k8s/to-override-default/values-test.yaml --install --cleanup-on-fail --dry-run --debug  > test-render.yaml
# ```
