#!/usr/bin/env python
# coding: utf-8

# # Pipeline

# ## Gitlab

# ### Set credential key in Gitlab Ci

# 
# * get the key like :
# ```json
# {
#   "type": "service_account",
#   "project_id": "foo",
#   "private_key_id": "XXX",
#   "private_key": "-----BEGIN PRIVATE KEY-----\YYY\n-----END PRIVATE KEY-----\n",
#   "client_email": "bar@foo.iam.gserviceaccount.com",
#   "client_id": "111",
#   "auth_uri": "https://accounts.google.com/o/oauth2/auth",
#   "token_uri": "https://oauth2.googleapis.com/token",
#   "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
#   "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/cloud-storage-admin%40steadysuncloud.iam.gserviceaccount.com"
# }
# ```
# 
# * encode this in `base 64`
# * put them in `CI/CD`/`Variables` link in key `BASE64_CREDENTIALS`
# * export them in ci.yml like:
# 
# ```yaml
# before_script:
#   - export BASE64_CREDENTIALS=$(echo $BASE64_CREDENTIALS | base64 -d)
# ```
# 

# ### Include other ci

# In `.gitlab-ci.yml`
# 
# ```yaml
# # Default Python CI file at me_project
# include:
#   - project: 'me_project/devops/ci-tools'
#     ref: master
#     file: 'python-default-ci.yml'
#   - project: 'me_project/devops/ci-tools'
#     ref: master
#     file: 'helm-default-ci.yml'
# ```
