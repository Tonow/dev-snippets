#!/usr/bin/env python
# coding: utf-8

# # Bash

# ## Loop
# 
# ```shell
# for i in $(ls folder/file_start_name*); do               
#     cmd $i             
# done
# 

# ## Cron

# https://crontab.guru/

# ## Grep

# [Regex check](https://regexr.com/)

# ```shell
# grep -nri -e "foo.*bar" --include="*py" .
# ```
