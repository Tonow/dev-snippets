#!/usr/bin/env python
# coding: utf-8

# # SSH

# ## Add multi keys

# ```shell
# ssh-add ~/.ssh/id_ed25519 ~/.ssh/id_ed25519_Tonow-perso-computer-work
# ```

# ## Connection

# ```shell
# ssh user@ip-or-host -p port-number
# ```

# ## Add host name

# Insert in `/etc/hosts`
# 
# ```shell
# xxx.x.x.x   my-super-host
# ```

# ## SCP

# ```shell
# scp local-file.txt user@ip-or-host:/folder/
# ```
