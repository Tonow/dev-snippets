#!/usr/bin/env python
# coding: utf-8

# # Git

# ## Rebase
# 
# ```shell
# git rebase -i HEAD~2 --autosquash
# ```

# ## Log
# 
# ```shell
# git log --all --graph --decorate --oneline
# ```
